package netcracker.intensive.rover;

import netcracker.intensive.rover.constants.CellState;

public class GroundCell {
    private CellState state;

    public GroundCell(CellState cellState) {
        state = cellState;
    }

    public CellState getState() {
        return state;
    }
}
