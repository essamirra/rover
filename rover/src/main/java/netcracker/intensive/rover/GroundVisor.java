package netcracker.intensive.rover;

import netcracker.intensive.rover.constants.CellState;

import static netcracker.intensive.rover.constants.CellState.OCCUPIED;

public class GroundVisor {

    private Ground observedField;
    public GroundVisor(Ground ground) {
        observedField = ground;
    }

    public boolean hasObstacles(Point point) throws IllegalArgumentException {
       return OCCUPIED.equals(observedField.getCell(point.getX(), point.getY()).getState());
    }
}
