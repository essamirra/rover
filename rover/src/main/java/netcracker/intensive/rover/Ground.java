package netcracker.intensive.rover;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Ground {
    //сначала строки, потом столбцы!!!!
    private GroundCell[][] field;

    public int getRows() {
        return rows;
    }

    private int rows;

    public int getCols() {
        return cols;
    }

    private int cols;

    public Ground(int rows, int cols) throws IllegalArgumentException {
        if(rows <= 0 || cols <= 0)
            throw new IllegalArgumentException("Ground parameters can not be negative");
        this.rows = rows;
        this.cols = cols;
        field = new GroundCell[rows][cols];
    }

    public void initialize(GroundCell... cells) {
        if(cells.length < rows*cols)
            throw new IllegalArgumentException("Not enough cells to initialize ground");
        for (int i = 0; i < rows ; i++) {
            for (int j = 0; j < cols ; j++) {
                field[i][j] = cells[cols*i +j];
            }
        }
    }
//клетки с 1????
    public GroundCell getCell(int x, int y) throws IllegalArgumentException {
        if(y >= rows || y < 0 || x>= cols || x < 0 )
            throw new IllegalArgumentException("Wrong indexes");
        return field[y][x];

    }
}
