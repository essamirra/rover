package netcracker.intensive.rover.stats;

import netcracker.intensive.rover.Point;

import java.util.Collection;

/**
 * Created by Proinina Maria
 */
public class ImmutableSimpleRoverStatsModule extends SimpleRoverStatsModule implements RoverStatsModule {
    private SimpleRoverStatsModule original;
    public ImmutableSimpleRoverStatsModule(SimpleRoverStatsModule stats)
    {
        original = stats;
    }

    @Override
    public void registerPosition(Point position) {
        original.registerPosition(position);
    }

    @Override
    public boolean isVisited(Point point) {
        return original.isVisited(point);
    }

    @Override
    public Collection<Point> getVisitedPoints() {
        return original.getVisitedPoints();
    }

    @Override
    public boolean get(String stats) {
        return original.get(stats);
    }

    @Override
    public void put(String name, Object value) {
        throw new UnsupportedOperationException();

    }
}
