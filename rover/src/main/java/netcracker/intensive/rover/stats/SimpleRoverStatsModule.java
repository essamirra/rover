package netcracker.intensive.rover.stats;

import netcracker.intensive.rover.Point;

import java.util.*;

public class SimpleRoverStatsModule implements RoverStatsModule {
    protected Set<Point> visitedPoints = new LinkedHashSet<>();
    protected Map<String, Object> settings = new HashMap<>();

    @Override
    public void registerPosition(Point position) {
        visitedPoints.add(position);
    }

    @Override
    public boolean isVisited(Point point) {

        return visitedPoints.contains(point);
    }

    @Override
    public Collection<Point> getVisitedPoints() {
        return Collections.unmodifiableSet(visitedPoints);
    }

    @Override
    public boolean get(String stats) {
        return (boolean) settings.get(stats);
    }

    @Override
    public void put(String name, Object value) {
        settings.put(name,value);
    }
}
