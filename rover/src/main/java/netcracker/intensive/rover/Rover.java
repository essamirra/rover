package netcracker.intensive.rover;

import netcracker.intensive.rover.constants.Direction;
import netcracker.intensive.rover.stats.ImmutableSimpleRoverStatsModule;
import netcracker.intensive.rover.stats.SimpleRoverStatsModule;


public class Rover implements Landable, Liftable, Moveable, Turnable {
    protected Point currentPosition;
    protected boolean airborne = false;
    protected GroundVisor visor;
    protected Direction direction;
    protected SimpleRoverStatsModule settings;
    protected ImmutableSimpleRoverStatsModule settingsToReturn;

    public Rover(GroundVisor groundVisor) {
        visor = groundVisor;
        currentPosition = new Point(0, 0);
        direction = Direction.SOUTH;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rover rover = (Rover) o;

        if (airborne != rover.airborne) return false;
        if (!currentPosition.equals(rover.currentPosition)) return false;
        if (!visor.equals(rover.visor)) return false;
        if (direction != rover.direction) return false;
        return settings.equals(rover.settings);

    }

    @Override
    public int hashCode() {
        int result = currentPosition.hashCode();
        result = 31 * result + (airborne ? 1 : 0);
        result = 31 * result + visor.hashCode();
        result = 31 * result + direction.hashCode();
        result = 31 * result + settings.hashCode();
        return result;
    }

    public void turnTo(Direction dir) {
        direction = dir;
    }

    public void move() {
        if (airborne) return;
        Point expectedPosition = currentPosition;
        switch (direction) {
            case NORTH:
                expectedPosition = new Point(currentPosition.getX(), currentPosition.getY() - 1);
                break;
            case EAST:
                expectedPosition = new Point(currentPosition.getX() + 1, currentPosition.getY());
                break;
            case SOUTH:
                expectedPosition = new Point(currentPosition.getX(), currentPosition.getY() + 1);
                break;
            case WEST:
                expectedPosition = new Point(currentPosition.getX() - 1, currentPosition.getY());
                break;
        }
        try {
            if (!visor.hasObstacles(expectedPosition)) {
                currentPosition = expectedPosition;
            }

        } catch (IllegalArgumentException e) {
            lift();
        }

    }

    public Point getCurrentPosition() {
        return currentPosition;
    }

    public boolean isAirborne() {
        return airborne;
    }

    public void lift() {
        airborne = true;
        currentPosition = null;
        direction = null;
    }

    @Override
    public void land(Point position, Direction dir) {
        if (!airborne) return;

        try {
            if (!visor.hasObstacles(position)) {
                airborne = false;
                currentPosition = position;
                direction = dir;
            } else {
                airborne = true;

            }
        } catch (IllegalArgumentException e) {
            airborne = true;
        }

    }

    public Direction getDirection() {
        return direction;
    }

    public SimpleRoverStatsModule getSettings() {
        return settingsToReturn;
    }
}
