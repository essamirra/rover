package netcracker.intensive.rover.programmable;

import netcracker.intensive.rover.GroundVisor;
import netcracker.intensive.rover.Point;
import netcracker.intensive.rover.Rover;
import netcracker.intensive.rover.command.RoverCommand;
import netcracker.intensive.rover.stats.ImmutableSimpleRoverStatsModule;
import netcracker.intensive.rover.stats.RoverStatsModule;
import netcracker.intensive.rover.stats.SimpleRoverStatsModule;

import java.util.Map;

/**
 * Этот класс должен уметь все то, что умеет обычный Rover, но при этом он еще должен уметь выполнять программы,
 * содержащиеся в файлах
 */
public class ProgrammableRover extends Rover implements ProgramFileAware {

    protected RoverCommandParser parser;


    public ProgrammableRover(GroundVisor groundVisor, SimpleRoverStatsModule simpleRoverStatsModule) {
        super(groundVisor);
        settings = simpleRoverStatsModule;
    }

    public ProgrammableRover(GroundVisor visor)
    {
        super(visor);
    }



    public void executeProgramFile(String file) {
        parser = new RoverCommandParser(this, file);
        RoverProgram program = parser.getProgram();
        Map<String, Object> settingsFromFile = program.getSettings();
        boolean isLogEnabled = false;
        boolean isStatsEnabled = false;
        if(settingsFromFile.containsKey(RoverProgram.LOG))
            isLogEnabled = (boolean)settingsFromFile.get((RoverProgram.LOG)) ;
        if(settingsFromFile.containsKey(RoverProgram.STATS))
            isStatsEnabled = (boolean)settingsFromFile.get(RoverProgram.STATS);
        settings.put(RoverProgram.LOG, isLogEnabled);
        settings.put(RoverProgram.STATS, isStatsEnabled);
        settingsToReturn = new ImmutableSimpleRoverStatsModule(settings);

        for (RoverCommand command:program.getCommands()
             ) {
            command.execute();
            if(isStatsEnabled)
                settings.registerPosition(currentPosition);
        }
        
    }

}
