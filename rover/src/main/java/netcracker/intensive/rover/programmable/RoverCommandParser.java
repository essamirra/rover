package netcracker.intensive.rover.programmable;

import netcracker.intensive.rover.Point;
import netcracker.intensive.rover.command.*;
import netcracker.intensive.rover.constants.Direction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class RoverCommandParser {
    private final String moveCommand = "move";
    private final String turnCommand = "turn";
    private final String liftCommand = "lift";
    private final String landCommand = "land";
    private boolean needLogging = false;
    private RoverProgram program;


    public RoverCommandParser(ProgrammableRover rover, String path) {
        Map<String, Direction> dirs = new HashMap<>(); dirs.put("north", Direction.NORTH);
        dirs.put("east", Direction.EAST);
        dirs.put("south", Direction.SOUTH);
        dirs.put("west", Direction.WEST);
        Map<String, Boolean> enablers = new HashMap<>(); enablers.put("on", true);
        enablers.put("off", false);


        try (BufferedReader file = new BufferedReader(new FileReader(path))) {
            Map<String, Object> settings = new HashMap<String, Object>();
            List<RoverCommand> commands = new LinkedList<>();
            String currentLine;
            ParserState state = ParserState.START;
            while ((currentLine = file.readLine()) != null) {
                switch (state) {

                    case START: {

                        String[] parts = currentLine.split(" ");
                        if (parts.length != 2) {
                            state = ParserState.ERROR;
                        } else if (parts[0].equals(RoverProgram.LOG)) {
                            state = ParserState.LOG;
                            if (enablers.containsKey(parts[1])) {
                                settings.put(RoverProgram.LOG, enablers.get(parts[1]));
                                needLogging = enablers.get(parts[1]);
                            }
                            else state = ParserState.ERROR;

                        } else if (parts[0].equals(RoverProgram.STATS)) {
                            state = ParserState.STATS;
                            if (enablers.containsKey(parts[1]))
                                settings.put(RoverProgram.STATS, enablers.get(parts[1]));
                            else state = ParserState.ERROR;
                        } else {
                            state = ParserState.ERROR;
                        }
                        break;
                    }
                    case LOG:
                        if (currentLine.equals("")) {
                            state = ParserState.SEPARATOR;
                        } else  {
                            String[] parts = currentLine.split(" ");
                            if (parts.length != 2) {
                                state = ParserState.ERROR;
                            } else if (parts[0].equals(RoverProgram.STATS)) {
                                state = ParserState.STATS;
                                if (enablers.containsKey(parts[1]))
                                    settings.put(RoverProgram.STATS, enablers.get(parts[1]));
                                else state = ParserState.ERROR;
                            } else {
                                state = ParserState.ERROR;
                            }

                        }

                        break;
                    case STATS:
                        if (currentLine.equals(RoverProgram.SEPARATOR))
                            state = ParserState.COMMAND;
                        else {
                            state = ParserState.ERROR;
                        }
                        break;

                    case COMMAND: {
                        String[] parts = currentLine.split(" ");
                        switch (parts.length) {
                            case 1:
                                switch (parts[0]) {
                                    case moveCommand:
                                        commands.add(needLogging(new MoveCommand(rover)));
                                        break;
                                    case liftCommand:
                                        commands.add(needLogging(new LiftCommand(rover)));
                                        break;
                                }
                                break;
                            case 2:
                                if (parts[0].equals(turnCommand)) {
                                    if (dirs.containsKey(parts[1])) {
                                        commands.add(needLogging(new TurnCommand(rover, dirs.get(parts[1]))));
                                    } else state = ParserState.ERROR;
                                } else
                                    state = ParserState.ERROR;
                                break;
                            case 4:
                                if (parts[0].equals(landCommand)) {
                                    try {
                                        int x = Integer.parseInt(parts[1]);
                                        int y = Integer.parseInt(parts[2]);
                                        if (dirs.containsKey(parts[3]))
                                            commands.add(needLogging(new LandCommand(rover, new Point(x, y), dirs.get(parts[3]))));
                                        else state = ParserState.ERROR;
                                    } catch (NumberFormatException e) {
                                        state = ParserState.ERROR;
                                    }
                                }
                                break;
                            default:
                                state = ParserState.ERROR;
                        }

                    }
                    break;
                    case ERROR:
                        throw new RoverCommandParserException();

                }

            }
            program = new RoverProgram(settings, commands);
            file.close();
        } catch (IOException e) {
            throw new RoverCommandParserException();
        }
    }

    private RoverCommand needLogging(RoverCommand moveCommand) {
        if(needLogging)
            return new LoggingCommand(moveCommand);
        else return moveCommand;
    }

    public RoverProgram getProgram() {
        return program;
    }


    private enum ParserState {
        START,
        LOG,
        STATS,
        SEPARATOR,
        EMPTYLINE,
        COMMAND,
        ERROR
    }
}
