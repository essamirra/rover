package netcracker.intensive.rover.programmable;

import com.sun.javafx.UnmodifiableArrayList;
import com.sun.javafx.collections.ImmutableObservableList;
import netcracker.intensive.rover.Rover;
import netcracker.intensive.rover.command.RoverCommand;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class RoverProgram {
    public static final String LOG = "log";
    public static final String STATS = "stats";
    public static final String SEPARATOR = "===";
    private Map<String,Object> settings;
    private List<RoverCommand> commands;
    public RoverProgram(Map<String, Object> stngs, List<RoverCommand> comms)
    {
        settings = stngs;
        commands = comms;
    }

    public Map<String, Object> getSettings() {
        return Collections.unmodifiableMap(settings);
    }

    public Collection<RoverCommand> getCommands() {
        return Collections.unmodifiableList(commands);
    }



}
