package netcracker.intensive.rover.command;

import netcracker.intensive.rover.Point;
import netcracker.intensive.rover.Rover;
import netcracker.intensive.rover.constants.Direction;

public class LandCommand implements RoverCommand {
    private Rover rover;
    private Point position;
    private Direction direction;

    @Override
    public String toString() {
        return "Land at (" + position.getX() + ", " + position.getY() +") heading " + direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LandCommand that = (LandCommand) o;

        if (!rover.equals(that.rover)) return false;
        if (!position.equals(that.position)) return false;
        return direction == that.direction;

    }

    @Override
    public int hashCode() {
        int result = rover.hashCode();
        result = 31 * result + position.hashCode();
        result = 31 * result + direction.hashCode();
        return result;
    }

    public LandCommand(Rover rover, Point position, Direction direction) {
        this.rover = rover;
        this.position = position;
        this.direction = direction;

    }

    @Override
    public void execute() {
        rover.land(position,direction);

    }
}
