package netcracker.intensive.rover.command;

import netcracker.intensive.rover.Rover;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingCommand implements RoverCommand {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingCommand.class);
    private RoverCommand originalCommand;

    public LoggingCommand(RoverCommand command) {
        originalCommand = command;
    }

    @Override
    public String toString() {
        return originalCommand.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoggingCommand that = (LoggingCommand) o;

        return originalCommand != null ? originalCommand.equals(that.originalCommand) : that.originalCommand == null;

    }

    @Override
    public int hashCode() {
        return originalCommand != null ? originalCommand.hashCode() : 0;
    }

    @Override
    public void execute() {
        LOGGER.debug(originalCommand.toString());

        originalCommand.execute();
    }
}
